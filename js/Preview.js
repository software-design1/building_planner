class Preview {
    constructor ()
    {
        this.scene=null;
        this.camera=null;
        this.controls=null;
        this.renderer=null;
        this.plane_area = {x:0, z:0};

        var container = document.getElementById("3d");

        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera( 45, 1, 1, 1000 );
        this.camera.position.set(0, 32, 16);

        this.controls = new THREE.OrbitControls( this.camera , container );

        /*var light = new THREE.DirectionalLight( 0xffffff, 2 );
        light.position.set( 1, 1, 1 );
        scene.add( light );*/
        this.renderer = new THREE.WebGLRenderer({alpha: true});
        this.renderer.setSize(window.innerWidth*0.3,window.innerHeight*0.9);
        this.renderer.setClearColor( 0xc2ffcf );
        container.appendChild( this.renderer.domElement );
    }

    addBlock(x, y, z, texture)
    {
        /* la posizione va da 0 alla dimensione del piano - 1
        ** n dimensione range = [0, n-1]
        */
        if (x>=0 && x<this.plane_area.x && z>=0 && z<this.plane_area.z && y>=0)
        {
            var textureLoader = new THREE.TextureLoader();

            var texture = textureLoader.load(texture);
            var geometry = new THREE.BoxGeometry( 1, 1, 1 );
            var material = new THREE.MeshBasicMaterial( { color: 0xffffff, map : texture, transparent: true } );
            var cube = new THREE.Mesh( geometry, material );
            cube.position.set(x-this.plane_area.x/2+0.5,y,z-this.plane_area.z/2+0.5);
            cube.name = x+','+y+','+z
            this.scene.add( cube );
        }
        //console.log(x+' '+y+' '+z);
    }

    removeBlock(x,y,z)
    {
        var cube = this.scene.getObjectByName(x+','+y+','+z);
        if (cube != undefined)
        {
            this.scene.remove(cube);
        }
    }

    addPlane(sizex, sizey)
    {
        this.cleanScene();
        this.plane_area.x = sizex;
        this.plane_area.z = sizey;
        var textureLoader = new THREE.TextureLoader();

        var texture = textureLoader.load('assets/blocks/grass_top.png');
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set( sizex, sizey );
        //texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
        texture.anisotropy = 0;
        var plane = new THREE.Mesh(new THREE.PlaneGeometry(sizex, sizey), new THREE.MeshBasicMaterial({ color: 0x538e49, map : texture }));
        plane.position.set(0,-0.5,0)
        plane.rotation.x = - Math.PI / 2;
        this.scene.add(plane);
        //console.log(sizex + ' ' + sizey);
    }

    cleanScene()
    {
        this.scene = new THREE.Scene();
    }
}

function render()
{
    preview.renderer.render( preview.scene, preview.camera );
}

function animate ()
{
    requestAnimationFrame( animate );
    render()
}

var preview = new Preview();
render();
animate();
