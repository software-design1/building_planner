var input = new Vue({
    el: "#input",
    data: {
        grid: {
            rows: 0,
            columns: 0,
            layers: 1,
        },
        structure: null,
        active_block: null,
    },

    methods: {
        clickBlock: function (x,y,z)
        {
            if (this.active_block != null)
            {
                var texture_path = 'assets/blocks/'+this.active_block.texture+'.png';
                preview.removeBlock(z,y,x);
                preview.addBlock(z,y,x, texture_path);
                this.structure[x][z] = Object.assign({}, this.active_block);
                this.$forceUpdate();
            } else {
                alert('No Block Selected');
            }

        },

        debug: function(message)
        {
            alert(message);
        },
    },

    watch: {

        'grid.rows': function (val, oldval)
        {
            this.grid.rows = Number(this.grid.rows);
            preview.addPlane(this.grid.rows,this.grid.columns);

            this.structure = new Array(this.grid.rows);
            for (var i=0; i<this.structure.length; i++)
            {
                this.structure[i] = new Array(this.grid.columns);
            }
        },

        'grid.columns': function (val, oldval)
        {
            this.grid.columns = Number(this.grid.columns);
            preview.addPlane(this.grid.rows,this.grid.columns);

            this.structure = new Array(this.grid.rows);
            for (var i=0; i<this.structure.length; i++)
            {
                this.structure[i] = new Array(this.grid.columns);
            }
        },

        'grid.layers': function (val, oldval)
        {
            this.grid.layers = Number(this.grid.layers);
            if (val<1)
            {
                this.grid.layers = 1;
            }
        },
    },
});
